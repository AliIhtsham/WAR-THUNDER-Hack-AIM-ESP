# WAR-THUNDER-Hack-AIM-ESP

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# AIM ASSIST

- Enabled | Enable AIM
- AIM key | Bind buttons AIM-a
- AIM radius | Radius of operation AIM-a
- AIM precision | AIM-a accuracy
- Moving adjustment | AIM-a lead adjustment for moving targets
- Switch spot key | Bind buttons for changing the firing point

# ESP

- Enabled | Enabling ESP
- Plane ESP | ESP on samotelty
- Tank ESP | ESP for tanks
- Bot ESP | ESP on bots
- Bomb ESP | ESP on bombs
- Rocket ESP | ESP for rockets
- Bounding box | Displaying a square on the opponent
- Vehicle name | Displaying the name of the enemy's equipment
- Vehicle direction | Direction of equipment
- Show missiles | Displaying missiles/ bombs
- Show ground units | display of ground equipment
- Ignore spotted | Don't display noticed opponents
- Ignore team | Do not display the command
- Render distance | Maximum enemy display distance
- Prediction (Tanks, planes, etc) | Lead point (Planes, tanks, etc.)
- Prediction spot | Choosing a point for the lead (Tower, hull, etc.)

# MISC

- HUD AIM Prediction | Enabling the prediction engine of the game itself
- Show gun ballistics | displaying the crosshair where the projectile will arrive (tanks only)
- Ship indicator | projectile indicator for ships
- Rocket indicator | the indicator was burned by rockets
- Gunner cam from sight | enabling the type of aiming by a machine gunner
- Show radar | Turn on the radar

# SOFT UI

- Hide overlay key | overlay hide button
- Save cpu | save CPU resources
- Radar radius | radar scale
- Radar X & Y | radar position

![5a5f9f9b9bd0c37aa96570c8e1eabcc4](https://user-images.githubusercontent.com/113304128/218262642-e1bf4930-2fee-48be-9d5a-34312a1c83d6.jpg)
![b6d9f6da2272399bbe48d717b2f7b068](https://user-images.githubusercontent.com/113304128/218262643-8456f4cf-35d8-4f99-88a5-c47dcbd7a2c6.jpg)
